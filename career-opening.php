<?php
/**
 * Template Name: Career Opening Template
 */

$context = Timber::get_context();

$context['post'] = Timber::get_post();

Timber::render( 'opening.twig', $context );
