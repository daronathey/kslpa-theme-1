<?php
/**
 * Template Name: Contact
 */

$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
$context['hero_image'] = $post->get_field('hero_image') ? new TimberImage($post->get_field('hero_image')) : $post->thumbnail();
$context['locations'] = Timber::get_posts( array( 'post_type' => 'location', 'posts_per_page' => '-1', 'orderby' => 'post_title' ) );
$context['employees'] = Timber::get_posts( array( 'post_type' => 'employee', 'posts_per_page' => '-1', 'order' => 'ASC' ) );

// Reorder the locations
$context['locations'][3] = $context['locations'][1];
unset($context['locations'][1]);

$context['employees_filtered'] = array();

foreach ( $context['locations'] as $location ){
    $context['employees_filtered'][$location->title] = array();
}

foreach ( $context['employees'] as $employee ){
    $context['employees_filtered'][$employee->get_field('location')->title][] = $employee;
}

Timber::render( 'contact.twig', $context );