<?php

$labels = array(
	'name'               => __( 'Employees', 'spha' ),
	'singular_name'      => __( 'Employee', 'spha' ),
	'add_new'            => _x( 'Add New Employee', 'spha', 'spha' ),
	'add_new_item'       => __( 'Add New Employee', 'spha' ),
	'edit_item'          => __( 'Edit Employee', 'spha' ),
	'new_item'           => __( 'New Employee', 'spha' ),
	'view_item'          => __( 'View Employee', 'spha' ),
	'search_items'       => __( 'Search Employees', 'spha' ),
	'not_found'          => __( 'No Employees found', 'spha' ),
	'not_found_in_trash' => __( 'No Employees found in Trash', 'spha' ),
	'parent_item_colon'  => __( 'Parent Employee:', 'spha' ),
	'menu_name'          => __( 'Employees', 'spha' ),
);

$args = array(
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => array(),
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => false,
	'menu_position'       => null,
	'menu_icon'           => 'dashicons-groups',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => true,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => array(
		'title', 'thumbnail'
	),
);

register_post_type( 'employee', $args );