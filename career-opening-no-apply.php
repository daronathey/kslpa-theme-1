<?php
/**
 * Template Name: Career Opening - No Apply
 */

$context = Timber::get_context();

$context['post'] = Timber::get_post();

Timber::render( 'opening-no-apply.twig', $context );